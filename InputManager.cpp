#include "InputManager.h"
#include "GameManager.h"


#include <iostream>
#include <sstream>
#include <string>
#include <cmath>
using namespace std;

InputManager::InputManager(GameManager* gm)
{
	game_manager = gm;
	input_manager_ois = NULL;
	
	keyboard_ois = NULL;
	mouse_ois = NULL;
	joystick_ois = NULL;
	
	init();
	
	window_width = game_manager->getRenderWindowWidth();
	window_height = game_manager->getRenderWindowHeight();
	
}

void InputManager::init()
{
	try
	{
		OIS::ParamList params;
		ostringstream windowHndStr;
		size_t window_handle = game_manager->getRenderWindowHandle();
		
		size_t handle = window_handle;
		windowHndStr << handle;
		params.insert(make_pair(string("WINDOW"), windowHndStr.str()));

		input_manager_ois = OIS::InputManager::createInputSystem(params);

		/*
		cout << "mice: " << input_manager_ois->getNumberOfDevices(OIS::OISMouse) << endl;
		
		cout << "keyboard: " << input_manager_ois->getNumberOfDevices(OIS::OISKeyboard) << endl;	
		*/
		if(input_manager_ois->getNumberOfDevices(OIS::OISMouse) > 0)
		{
			mouse_ois = static_cast<OIS::Mouse*>(input_manager_ois->createInputObject(OIS::OISMouse, true));
			mouse_ois->setEventCallback(this);
		}
		
		if(input_manager_ois->getNumberOfDevices(OIS::OISKeyboard) > 0)
		{
			keyboard_ois = static_cast<OIS::Keyboard*>(input_manager_ois->createInputObject(OIS::OISKeyboard, true));
			keyboard_ois->setEventCallback(this);
		}
		
		if(input_manager_ois->getNumberOfDevices(OIS::OISJoyStick) > 0)
		{
			joystick_ois = static_cast<OIS::JoyStick*>(input_manager_ois->createInputObject(OIS::OISJoyStick, true));
			joystick_ois->setEventCallback(this);
		}
		
	}
	
	catch(std::exception& e)
	{
		ASSERT_CRITICAL(false, e.what());
	}
	catch(...)
	{
		ASSERT_CRITICAL(false, "Input Manager Initialization Error");
	}
	
}

void InputManager::checkForInput(float time_step)
{
	if(keyboard_ois) 
	{
		keyboard_ois->capture();
	}
	
	if(mouse_ois) 
	{
		mouse_ois->capture();
	}
	
	if(joystick_ois)
	{
		joystick_ois->capture();
		OIS::JoyStickState state = joystick_ois->getJoyStickState();
		OIS::JoyStickEvent e(joystick_ois, state);
		axisMoved(e, 0);
	}
}

bool InputManager::keyPressed(const OIS::KeyEvent& e)
{
	game_manager->keyPressed(keyMap(e));
	return true;
}

bool InputManager::keyReleased(const OIS::KeyEvent& e)
{
//	game_manager->keyReleased(keyMap(e));
	return true;
}

bool InputManager::mousePressed(const OIS::MouseEvent& e, OIS::MouseButtonID button)
{
	
	uint32 x_click = e.state.X.abs;
	uint32 y_click = e.state.Y.abs;
	game_manager->mousePressed(x_click, y_click, mouseMap(button));
	return true;
}

bool InputManager::mouseReleased(const OIS::MouseEvent& e, OIS::MouseButtonID button)
{
	return true;
}

bool InputManager::mouseMoved(const OIS::MouseEvent& e)
{
	uint32 x_click = e.state.X.abs;
	uint32 y_click = e.state.Y.abs;
	int x_rel = (int) e.state.X.rel;
	int y_rel = (int) e.state.Y.rel;
	game_manager->mouseMoved(x_click, y_click, x_rel, y_rel);
	return true;
}

void InputManager::free()
{
	if(input_manager_ois)
	{
		input_manager_ois->destroyInputSystem(input_manager_ois);
	}
}

InputManager::~InputManager()
{
	free();
	
	input_manager_ois = NULL;
	keyboard_ois = NULL;
	mouse_ois = NULL;
	joystick_ois = NULL;

}

bool InputManager::buttonReleased(const OIS::JoyStickEvent& e, int button)
{
	//cout << e.state.mButtons[0] << "    " << e.state.mButtons[1] << endl;
	return true;
}

bool InputManager::buttonPressed(const OIS::JoyStickEvent& e, int button)
{
	game_manager->buttonPressed(joystickButtonMap(button));
	return true;
}

bool InputManager::sliderMoved(const OIS::JoyStickEvent& e, int index)
{
	//cout << index << endl;
	return true;
}

bool InputManager::povMoved(const OIS::JoyStickEvent& e, int index)
{
	
	return true;
}

bool InputManager::vector3Moved(const OIS::JoyStickEvent& e, int index)
{
	//cout << index << endl;
	return true;
}

bool InputManager::axisMoved(const OIS::JoyStickEvent& e, int axis)
{
	int TOL = 10000;
	float how_much[5] = {e.state.mAxes[0].abs, e.state.mAxes[1].abs, e.state.mAxes[2].abs,
						 e.state.mAxes[3].abs, e.state.mAxes[4].abs};
	//cout <<  how_much[2] << "       " << how_much[3] << endl;
	if(fabs(how_much[2]) > TOL || fabs(how_much[3]) > TOL)
	{
		game_manager->rightJoystickMoved(how_much[2], how_much[3]);
	}
	if(fabs(how_much[0]) > TOL || fabs(how_much[1]) > TOL)
	{
		game_manager->leftJoystickCallback(how_much[0], how_much[1], "./assets/scripts/left_joystick_moved.lua");
		//game_manager->leftJoystickMoved(how_much[0], how_much[1]);
	}
	return true;
}

string InputManager::joystickAxisMap(int axis)
{
	if(axis == 0)
	{
		return "LEFT_NS";
	}
	else if(axis == 1)
	{
		return "LEFT_EW";
	}
	else if(axis == 2)
	{
		return "RIGHT_NS";
	}
	else if(axis == 3)
	{
		return "RIGHT_EW";
	}
	else if(axis == 4)
	{
		return "TRIGGER";
	}
}

string InputManager::joystickButtonMap(int button)
{
	if(button == 0)
	{
		return "A";
	}
	else if(button == 1)
	{
		return "B";
	}
	else if(button == 2)
	{
		return "X";
	}
	else if(button == 3)
	{
		return "Y";
	}
	else if(button == 4)
	{
		return "LB";
	}
	else if(button == 5)
	{
		return "RB";
	}
	else if(button == 6)
	{
		return "SELECT";
	}
	else if(button == 7)
	{
		return "START";
	}
	else if(button == 8)
	{
		return "LJS";
	}
	else if(button == 9)
	{
		return "RJS";
	}
}

string InputManager::mouseMap(const OIS::MouseButtonID id)
{
	
	if(id == OIS::MB_Left)
	{
		return "left";
	}
	
	else if(id == OIS::MB_Right)
	{
		return "right";
	}
	else
	{
		return "middle";
	}
	
	
}

string InputManager::keyMap(const OIS::KeyEvent& e)
{
	string game_key = "INVALID KEY";
	OIS::KeyCode ois_code = e.key;
	
	if(ois_code == OIS::KC_ESCAPE)
	{
		game_key = "ESCAPE";
	}
	else if(ois_code == OIS::KC_0)
	{
		game_key = "0";
	}
	else if(ois_code == OIS::KC_1)
	{
		game_key = "1";
	}
	else if(ois_code == OIS::KC_2)
	{
		game_key = "2";
	}
	else if(ois_code == OIS::KC_3)
	{
		game_key = "3";
	}
	else if(ois_code == OIS::KC_4)
	{
		game_key = "4";
	}
	else if(ois_code == OIS::KC_5)
	{
		game_key = "5";
	}
	else if(ois_code == OIS::KC_6)
	{
		game_key = "6";
	}
	else if(ois_code == OIS::KC_7)
	{
		game_key = "7";
	}
	else if(ois_code == OIS::KC_8)
	{
		game_key = "8";
	}
	else if(ois_code == OIS::KC_9)
	{
		game_key = "9";
	}
	else if(ois_code == OIS::KC_A)
	{
		game_key = "A";
	}
	else if(ois_code == OIS::KC_W)
	{
		game_key = "W";
	}
	else if(ois_code == OIS::KC_S)
	{
		game_key = "S";
	}
	else if(ois_code == OIS::KC_D)
	{
		game_key = "D";
	}
	else if(ois_code == OIS::KC_RIGHT)
	{
		game_key = "RIGHT";
	}
	else if(ois_code == OIS::KC_LEFT)
	{
		game_key = "LEFT";
	}
	else if(ois_code == OIS::KC_UP)
	{
		game_key = "UP";
	}
	else if(ois_code == OIS::KC_DOWN)
	{
		game_key = "DOWN";
	}
	
	return game_key;
}










