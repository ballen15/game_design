#if !defined GUI_MANAGER
#define GUI_MANAGER

#include "GameHeader.h"
#include "GameManager.h"
#include "CEGUI/CEGUI.h"
#include "CEGUI/RendererModules/Ogre/Renderer.h"

class RenderManager;

class GUIManager
{
	private:
		RenderManager* render_manager;		
		CEGUI::OgreRenderer* cegui_renderer;		
		CEGUI::GUIContext* gui_context;
		CEGUI::Window* root_window;
		
		void createContext();
		void destroyContext();
		static CEGUI::OgreRenderer* getRenderer();
		
	public:
	
	GUIManager(RenderManager* rm);
	virtual ~GUIManager();
	
	void loadLevel(std::string level_name);
	void unloadLevel();
	void createComboBox();
	
	void keyPressed(std::string game_key);
	void mousePressed(uint32 abs_x, uint32 abs_y, std::string button);
	void mouseMoved(uint32 abs_x, uint32 abs_y, int rel_x, int rel_y);
	void buttonEvent(const CEGUI::EventArgs& e);
	void resetEvent(const CEGUI::EventArgs& e);
		

};

#endif