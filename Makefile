AutomatedMakefile = am
CC = g++

INC_DIRS=-I./ -I$(OGRE_PATH)/OgreMain/include -I$(BULLET_PATH)/src -I$(LUA_PATH)/src -I$(LUA_PATH)/luabridge -I$(BOOST_PATH) -I$(TXML_PATH)/include -I$(OIS_PATH)/include -I$(BASS_PATH)/include -I$(CEGUI_PATH)/cegui/include
LIB_DIRS=-L./ -L$(OGRE_PATH)/build/lib -L$(LUA_PATH)/lib -L$(BULLET_PATH)/lib -L$(BOOST_PATH)/stage/lib -L$(TXML_PATH)/lib -L$(OIS_PATH)/lib -L$(BASS_PATH)/lib -L$(CEGUI_PATH)/build/lib
LIBS=-lboost_system-mgw51-mt-1_63 -ltinyxml -lois -lbass -llua -lOgreMain -lBulletDynamics -lBulletCollision -lLinearMath -lCEGUIBase-0 -lCEGUIOgreRenderer-0

COMPILE = $(CC) $(INC_DIRS) -c
LINK = $(CC) $(LIB_DIRS) -o

FILES = GameDriver.o GameManager.o RenderManager.o AnimationRenderListener.o Utils.o RenderListener.o LogManager.o GameResource.o \
MeshResource.o PathResource.o GameXMLResources.o ResourceManager.o InputRenderListener.o InputManager.o AudioManager.o AudioResource.o \
GUIManager.o ScriptManager.o 
#RigidBody.o CompoundShape.o CompareCompoundShapes.o BulletSceneNodeMotionState.o BulletDebugDrawer.o

all: Ogre

Ogre:	$(FILES)
	$(LINK) Game.exe $(FILES) $(LIBS)
	
GameManager.o:					GameManager.h GameManager.cpp
								$(COMPILE) GameManager.cpp
							
#RigidBody.o:					RigidBody.h Rigidbody.cpp
#								$(COMPILE) RigidBody.cpp
							
#CompoundShape.o:				CompoundShape.h CompoundShape.cpp
#								$(COMPILE) CompoundShape.cpp
							
#CompareCompoundShapes.o:		CompareCompoundShapes.h CompareCompoundShapes.cpp
#								$(COMPILE) CompareCompoundShapes.cpp
							
#BulletSceneNodeMotionState.o:	BulletSceneNodeMotionState.h BulletSceneNodeMotionState.cpp
#								$(COMPILE) BulletSceneNodeMotionState.cpp
								
#BulletDebugDrawer.o:			BulletDebugDrawer.h BulletDebugDrawer.cpp
#								$(COMPILE) BulletDebugDrawer.cpp
						
ScriptManager.o:				ScriptManager.h ScriptManager.cpp
								$(COMPILE) ScriptManager.cpp
							
RenderListener.o:				RenderListener.h RenderListener.cpp
								$(COMPILE) RenderListener.cpp
							
AnimationRenderListener.o:		AnimationRenderListener.h AnimationRenderListener.cpp
								$(COMPILE) AnimationRenderListener.cpp
							
InputRenderListener.o:			InputRenderListener.h InputRenderListener.cpp
								$(COMPILE) InputRenderListener.cpp
							
InputManager.o:					InputManager.h InputManager.cpp
								$(COMPILE) InputManager.cpp
							
GUIManager.o:					GUIManager.h GUIManager.cpp
								$(COMPILE) GUIManager.cpp
							
AudioManager.o:					AudioManager.h AudioManager.cpp
								$(COMPILE) AudioManager.cpp
							
GameResource.o:					GameResource.h GameResource.cpp
								$(COMPILE) GameResource.cpp
							
AudioResource.o:				AudioResource.h AudioResource.cpp
								$(COMPILE) AudioResource.cpp
							
MeshResource.o:					MeshResource.h MeshResource.cpp
								$(COMPILE) MeshResource.cpp
							
PathResource.o:					PathResource.h PathResource.cpp
								$(COMPILE) PathResource.cpp
							
GameXMLResources.o:				GameXMLResources.h GameXMLResources.cpp
								$(COMPILE) GameXMLResources.cpp
							
ResourceManager.o:				ResourceManager.h ResourceManager.cpp
								$(COMPILE) ResourceManager.cpp
							
LogManager.o:					LogManager.h LogManager.cpp
								$(COMPILE) LogManager.cpp
					
RenderManager.o:				RenderManager.h RenderManager.cpp
								$(COMPILE) RenderManager.cpp
					
GameDriver.o:					GameDriver.cpp
								$(COMPILE) GameDriver.cpp
							
Utils.o:						Utils.h Utils.cpp
								$(COMPILE) Utils.cpp

