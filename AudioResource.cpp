#include "AudioResource.h"
#include "GameManager.h"
#include <cstdlib>
using namespace std;


AudioResource::AudioResource(string level_name, string file_name, string audio_name, int audio_type) : GameResource(level_name, file_name, AUDIO)
{
	//1 for sample, 2 for stream
	type = audio_type;
	name = audio_name;
}

AudioResource::~AudioResource()
{
	
}

string AudioResource::getName()
{
	return name;
}

int AudioResource::getType()
{
	return type;
}

AR_Info* AudioResource::getAudioInfo()
{
	return arinfo;
}

void AudioResource::load(GameManager* game_manager)
{
	arinfo = game_manager->createAudioInfo();
	game_manager->loadAudio(getFileName(), arinfo, type);
	
}

void AudioResource::unload(GameManager* game_manager)
{
	game_manager->unloadAudio(arinfo, type);
	
	free(arinfo);
}










