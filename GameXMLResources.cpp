#include "GameXMLResources.h"
#include "GameManager.h"
#include "Utils.h"

#include "PathResource.h"
#include "MeshResource.h"

#include <iostream>
#include <string>
using namespace std;


GameXMLResources::GameXMLResources(const char* resources_filename, const char* scenes_filename)
{
	resources_document = TiXmlDocument(resources_filename);
	scenes_document = TiXmlDocument(scenes_filename);
	bool success = resources_document.LoadFile();
	if(!success)
	{
		throw runtime_error("Error parsing resources");
	}
	success = scenes_document.LoadFile();
	if(!success)
	{
		throw runtime_error("Error parsing scene");
	}
	
}

GameXMLResources::~GameXMLResources()
{
}

void GameXMLResources::buildXMLScene(string requested_level, GameManager* game_manager)
{
	
	
	TiXmlElement* levels_element = scenes_document.RootElement();
	TiXmlElement* level_element = levels_element->FirstChildElement("level");
	
	while(level_element != NULL)
	{
		TiXmlElement* level_name_element = level_element->FirstChildElement("name");
		string level_name = level_name_element->GetText();
		if(level_name == requested_level)
		{
		
			TiXmlElement* camera_element = level_element->FirstChildElement("camera");
			TiXmlElement* camera_name_element = camera_element->FirstChildElement("name");
			string camera_name = camera_name_element->GetText();
			
			TiXmlElement* camera_position_element = camera_element->FirstChildElement("position");
			string camera_position_str = camera_position_element->GetText();
			float camera_position[3];
			Utils::parseFloats(camera_position_str, camera_position);
			
			TiXmlElement* camera_lookat_element = camera_element->FirstChildElement("lookat");
			string camera_lookat_str = camera_lookat_element->GetText();
			float camera_lookat[3];
			Utils::parseFloats(camera_lookat_str, camera_lookat);
			
			TiXmlElement* camera_clip_element = camera_element->FirstChildElement("clip");
			string camera_clip_str = camera_clip_element->GetText();
			float camera_clip[2];
			Utils::parseFloats(camera_clip_str, camera_clip);
			
			game_manager->setupCamera(camera_name, camera_position, camera_lookat, camera_clip);
			
			TiXmlElement* light_element = level_element->FirstChildElement("light");
			while(light_element != NULL)
			{
				TiXmlElement* light_name_element = light_element->FirstChildElement("name");
				string light_name = light_name_element->GetText();
				
				TiXmlElement* light_type_element = light_element->FirstChildElement("type");
				string light_type = light_type_element->GetText();
				
				TiXmlElement* light_color_element = light_element->FirstChildElement("color");
				string light_color_str = light_color_element->GetText();
				float light_color[3];
				Utils::parseFloats(light_color_str, light_color);
				
				TiXmlElement* light_direction_element = light_element->FirstChildElement("direction");
				string light_direction_str = light_direction_element->GetText();
				float light_direction[3];
				Utils::parseFloats(light_direction_str, light_direction);
				
				game_manager->setupLight(light_name, light_type, light_color, light_direction);
				
				light_element = light_element->NextSiblingElement("light");
			}
			TiXmlElement* graph_element = level_element->FirstChildElement("graph");
			TiXmlElement* children_element = graph_element->FirstChildElement("children");
			if(children_element != NULL)
			{
				processChildren(children_element, "root", game_manager);
			}
		}
		level_element = level_element->NextSiblingElement();
	}
}

void GameXMLResources::processChildren(TiXmlElement* children_element, string parent_name, GameManager* game_manager)
{
	TiXmlElement* child_element = children_element->FirstChildElement("child");
	
	while(child_element != NULL)
	{
		TiXmlElement* node_name_element = child_element->FirstChildElement("name");
		string node_name = node_name_element->GetText();
		
		game_manager->createSceneNode(node_name, parent_name);
		
		/*
		Ogre::SceneNode* scene_node = scene_manager->createSceneNode(node_name);
		parent_node->addChild(scene_node);
		*/
		TiXmlElement* entity_element = child_element->FirstChildElement("entity");
		if(entity_element != NULL)
		{
			TiXmlElement* entity_name_element = entity_element->FirstChildElement("name");
			string entity_name = entity_name_element->GetText();
			TiXmlElement* entity_mesh_element = entity_element->FirstChildElement("mesh");
			string mesh_name = entity_mesh_element->GetText();
			TiXmlElement* entity_materials_element = entity_element->FirstChildElement("materials");
			string materials_name = entity_materials_element->GetText();
			
			game_manager->createEntity(entity_name, node_name, mesh_name, materials_name);
			
			/*
			Ogre::Entity* entity = scene_manager->createEntity(entity_name, mesh_name);
			entity->setMaterialName(materials_name);
			scene_node->attachObject(entity);
			*/
		}
		
		TiXmlElement* animation_element = child_element->FirstChildElement("animation");
		
		if(animation_element != NULL)
		{
			processAnimation(animation_element, node_name, game_manager);
		}
		
		
		TiXmlElement* translate_element = child_element->FirstChildElement("translation");
		string translate_str = translate_element->GetText();
		float translate[3];
		Utils::parseFloats(translate_str, translate);		
		game_manager->processTransformations(1, translate, node_name);
		
		TiXmlElement* rotation_element = child_element->FirstChildElement("rotation");
		while(rotation_element != NULL)
		{
			string rotate_str = rotation_element->GetText();
			float rotate[4];
			Utils::parseFloats(rotate_str, rotate);
			game_manager->processTransformations(2, rotate, node_name);
			rotation_element = rotation_element->NextSiblingElement("rotation");
		}
		
		TiXmlElement* scale_element = child_element->FirstChildElement("scale");
		string scale_str = scale_element->GetText();
		float scale[3];
		Utils::parseFloats(scale_str, scale);
		game_manager->processTransformations(3, scale, node_name);
		
		TiXmlElement* grand_children_element = child_element->FirstChildElement("children");
		if(grand_children_element != NULL)
		{
			processChildren(grand_children_element, node_name, game_manager);
		}
		child_element = child_element->NextSiblingElement();
	}
}

void GameXMLResources::processAnimation(TiXmlElement* animation_element, string animation_node, GameManager* game_manager)
{
	
	TiXmlElement* animation_name_element = animation_element->FirstChildElement("name");
	string animation_name = animation_name_element->GetText();
	cout << animation_name << endl << endl;
	TiXmlElement* animation_mode_element = animation_element->FirstChildElement("mode");
	string animation_mode = animation_mode_element->GetText();
	TiXmlElement* animation_time_element = animation_element->FirstChildElement("seconds");
	string animation_time_str = animation_time_element->GetText();
	float animation_time = Utils::parseFloat(animation_time_str);
	game_manager->createAnimation(animation_name, animation_time, animation_mode, animation_node);
	
//	Ogre::NodeAnimationTrack* animation_track = myAnimation->createNodeTrack(1, animation_node);
//	Ogre::TransformKeyFrame* keyframe;
	
	TiXmlElement* animation_keyframes_element = animation_element->FirstChildElement("keyframes");
	TiXmlElement* animation_keyframe_element = animation_keyframes_element->FirstChildElement("keyframe");
	float keyframe_time = 0;
	while(animation_keyframe_element != NULL)
	{
		TiXmlElement* keyframe_time_element = animation_keyframe_element->FirstChildElement("time");
		string keyframe_time_str = keyframe_time_element->GetText();
		keyframe_time = Utils::parseFloat(keyframe_time_str);
//		keyframe = animation_track->createNodeKeyFrame(keyframe_time);
		
		TiXmlElement* keyframe_trans_element = animation_keyframe_element->FirstChildElement("translation");
		string keyframe_trans_str = keyframe_trans_element->GetText();
		float key_translate[3];
		Utils::parseFloats(keyframe_trans_str, key_translate);		
//		keyframe->setTranslate(Ogre::Vector3(key_translate[0], key_translate[1], key_translate[2]));
		
		
		TiXmlElement* keyframe_rot_element = animation_keyframe_element->FirstChildElement("rotation");
		string keyframe_rot_str = keyframe_rot_element->GetText();
		float key_rotate[4];
		Utils::parseFloats(keyframe_rot_str, key_rotate);
//		keyframe->setRotation(Ogre::Quaternion(Ogre::Degree(key_rotate[0]), Ogre::Vector3(key_rotate[1], key_rotate[2], key_rotate[3])));
		
		
		TiXmlElement* keyframe_scale_element = animation_keyframe_element->FirstChildElement("scale");
		string keyframe_scale_str = keyframe_scale_element->GetText();
		float key_scale[3];
		Utils::parseFloats(keyframe_scale_str, key_scale);		
//		keyframe->setScale(Ogre::Vector3(key_scale[0], key_scale[1], key_scale[2]));
		
		game_manager->createKeyFrame(animation_name, keyframe_time, key_translate, key_rotate, key_scale);
		
		animation_keyframe_element = animation_keyframe_element->NextSiblingElement();
	}
	game_manager->createAnimationState(animation_name);
//	Ogre::AnimationState* myAnimationState = scene_manager->createAnimationState(animation_name);
//	myAnimationState->setEnabled(true);
//	myAnimationState->setLoop(false);
	
//	animation_states->add(myAnimationState);
	
}

ListArray<GameResource>* GameXMLResources::getResourcesByLevel(string requested_level, GameManager* game_manager)
{
	ListArray<GameResource>* game_resources = new ListArray<GameResource>();
	
	TiXmlElement* levels_element = resources_document.RootElement();
	TiXmlElement* level_element = levels_element->FirstChildElement("level");
	
	while(level_element != NULL)
	{
		TiXmlElement* level_name_element = level_element->FirstChildElement();
		string level_name = level_name_element->GetText();
		if(requested_level == level_name)
		{
			
			TiXmlElement* paths_element = level_element->FirstChildElement("paths");
			TiXmlElement* path_element = paths_element->FirstChildElement("path");
		
			while(path_element != NULL)
			{
				string path = path_element->GetText();
				PathResource* path_resource = new PathResource(requested_level, path, PATH, game_manager);
				game_resources->add(path_resource);
				game_manager->addPathResource(path, requested_level);
				path_element = path_element->NextSiblingElement();
			}
		
			TiXmlElement* meshes_element = level_element->FirstChildElement("meshes");
			TiXmlElement* mesh_element = meshes_element->FirstChildElement("mesh");
		
			while(mesh_element != NULL)
			{
				string mesh = mesh_element->GetText();
				MeshResource* mesh_resource = new MeshResource(requested_level, mesh, MESH, game_manager);
				game_resources->add(mesh_resource);
				game_manager->addMeshResource(mesh, requested_level);
				mesh_element = mesh_element->NextSiblingElement();
			}
			
			TiXmlElement* audio_element = level_element->FirstChildElement("audio");
			int audio_type = 0;
			while(audio_element != NULL)
			{
				TiXmlElement* audio_file_element = audio_element->FirstChildElement("file");
				string audio_file = audio_file_element->GetText();
				TiXmlElement* audio_name_element = audio_element->FirstChildElement("name");
				string audio_name = audio_name_element->GetText();
				TiXmlElement* audio_type_element = audio_element->FirstChildElement("type");
				string audio_type_str = audio_type_element->GetText();
				audio_type = Utils::parseInt(audio_type_str);
				
				game_manager->addAudioResource(requested_level, audio_file, audio_name, audio_type);
				audio_element = audio_element->NextSiblingElement("audio");
			}
			
		}
		level_element = level_element->NextSiblingElement();
	}
	
	return game_resources;
}