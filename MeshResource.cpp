#include "MeshResource.h"
#include "GameManager.h"

MeshResource::MeshResource(string level_name, string mesh, GameResourceType type, GameManager* gm) : GameResource(level_name, mesh, type)
{
	game_manager = gm;
}

MeshResource::~MeshResource()
{
}

void MeshResource::load()
{
	game_manager->addMeshResource(getFileName(), getLevelName());
	is_loaded = true;
}

void MeshResource::unload()
{
	is_loaded = false;
}
