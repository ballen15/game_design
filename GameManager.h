#if !defined GAME_MANAGER
#define GAME_MANAGER

#include <string>
#include "LogManager.h"
#include "CSC2110/ListArray.h"

typedef unsigned int uint32;

class RenderManager;
class ResourceManager;
class InputManager;
class GUIManager;
class AudioManager;
class AudioResource;
class ScriptManager;
struct AR_Info;


//supplies communication between managers
class GameManager
{
   private:
      RenderManager* render_manager;
	  LogManager* log_manager;
	  ResourceManager* resource_manager; 
	  InputManager* input_manager;
	  GUIManager* gui_manager;
	  AudioManager* audio_manager;
	  ScriptManager* script_manager;

      GameManager();
      void init();

   public:
      virtual ~GameManager();
      static GameManager* getGameManager();

	  void addPathResource(std::string path, std::string level_name);
	  void addMeshResource(std::string mesh, std::string level_name);
	  void addAudioResource(std::string level_name, std::string file_name, std::string name, int type);
	  void unloadLevel(std::string level_name);
	  void loadLevel(std::string level_name);
	  void setupCamera(std::string cam_name, float* cam_position, float* cam_lookat, float* cam_clip);
	  void setupLight(std::string light_name, std::string type, float* light_color, float* light_direction);
	  void createEntity(std::string entity_name, std::string node_name, std::string mesh_name, std::string materials_name);
	  void createSceneNode(std::string child_node, std::string parent_node);
	  void processTransformations(int type, float* transform_values, std::string node_name);
	  void createAnimation(std::string animation_name, float animation_time, std::string animation_mode, std::string animation_node);
	  void createKeyFrame(std::string animation_name, float keyframe_time, float* key_translate, float* key_rotate, float* key_scale);
	  void createAnimationState(std::string animation_name);
	  void checkForInput(float time_step);
	  void mousePressed(uint32 x_click, uint32 y_click, std::string mouseMap);
	  void mouseMoved(uint32 x_click, uint32 y_click, int x_rel, int y_rel);
	  void keyPressed(std::string keyMap);
	  void buttonPressed(std::string buttonMap);
	  void rightJoystickMoved(float ns, float ew);
	  void leftJoystickMoved(float ns, float ew);
	  void leftJoystickCallback(float ns, float ew, std::string event_script);
	  AR_Info* createAudioInfo();
	  void loadAudio(std::string file_name, AR_Info* arinfo, int type);
	  void unloadAudio(AR_Info* arinfo, int type);
	  void playAudio(std::string audio_name);
	  void updateAudio(float time_step);
	  void loadGUI(std::string level_name);
	  ListArray<AudioResource>* getAudioResources();

      int getRenderWindowWidth();
      int getRenderWindowHeight();
      size_t getRenderWindowHandle();
	  std::string getLoadedLevelName();
	  void logProblem(std::string msg, std::string file, int line);
	  void logComment(std::string comment);
};

#endif
