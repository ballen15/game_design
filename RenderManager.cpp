#include "RenderManager.h"
#include "GameManager.h"
#include "CSC2110/ListArrayIterator.h"
#include "GameHeader.h"
#include "AudioResource.h"
#include "ScriptManager.h"

#include <iostream>
#include <cmath>
using namespace std;

struct SceneNodeMotion
{
	Ogre::SceneNode* scene_node_motion;
};

struct SceneNodeManual
{
	Ogre::SceneNode* scene_node_manual;
};

void RenderManager::init()
{
	root = NULL;
	window = NULL;
	scene_manager = NULL;
	
	try
	{
	root = OGRE_NEW Ogre::Root("","");  //resource/config files go here
	root->loadPlugin("RenderSystem_GL");  //prepares external dlls for later use

    Ogre::RenderSystem* render_system = root->getRenderSystemByName("OpenGL Rendering Subsystem"); //just returns a pointer to an uninitialized render system
    if (!render_system)
    {
       ASSERT_CRITICAL(false, "Render System failed!");
    }
    root->setRenderSystem(render_system);
    //manually set configuration options
    render_system->setConfigOption("Full Screen", "No");
    render_system->setConfigOption("Video Mode", "1200 x 720 @ 32-bit colour");

	//initialize render system
    //automatically create the window and give it a title
    window = root->initialise(true, "Game Engine Programming");  

    scene_manager = root->createSceneManager(Ogre::ST_GENERIC, "Default Scene Manager");
    window->getCustomAttribute("WINDOW", &window_handle);
	
	viewport = window->addViewport(NULL, 1, 0, 0, 1, 1);
	viewport->setBackgroundColour(Ogre::ColourValue(.47,.7,1));
	}
	
	catch(Ogre::Exception& e)
	{
		ASSERT_CRITICAL(false, e.what());
	}

    //the Ogre viewport corresponds to a clipping region into which the contents of the camera view will be rendered in the window on each frame
    //by default, the size of the viewport matches the size of the window, but the viewport can be cropped
    //the camera represents a view into an existing scene and the viewport represents a region into which an existing camera will render its contents
      
}

RenderManager::RenderManager(GameManager* gm)
{
   game_manager = gm;
   script_manager = new ScriptManager(gm);
   init();
   
   animation_states = new ListArray<Ogre::AnimationState>();
   render_listeners = new ListArray<RenderListener>();
   audio_resources = new ListArray<AudioResource>();
   animation_render_listener = new AnimationRenderListener(this);
   render_listeners->add(animation_render_listener);
   input_render_listener = new InputRenderListener(this);
   render_listeners->add(input_render_listener);
   root->addFrameListener(animation_render_listener);
   root->addFrameListener(input_render_listener);
}

RenderManager::~RenderManager()
{
   game_manager = NULL;
   delete script_manager;
   script_manager = NULL;
   delete animation_render_listener;
   animation_render_listener = NULL;
   /*
   ListArrayIterator<Ogre::AnimationState>* a_iter = animation_states->iterator();
   while(a_iter->hasNext())
   {
      Ogre::AnimationState* anim_state = a_iter->next();
	  delete anim_state;
   }
   delete a_iter;
   */
   delete animation_states;
   /*
   ListArrayIterator<RenderListener>* r_iter = render_listeners->iterator();
   while(r_iter->hasNext())
   {
      RenderListener* render_listener = r_iter->next();
	  delete render_listener;
   }
   delete r_iter;
   */
   delete render_listeners;
   
   delete audio_resources;
   
   scene_manager->clearScene();
   scene_manager->destroyAllCameras();

   window->removeAllViewports();

   window->destroy();
   window = NULL;

   delete root;
   root = NULL;
}

size_t RenderManager::getRenderWindowHandle()
{
   return window_handle;
}

int RenderManager::getRenderWindowWidth()
{
   return viewport->getActualWidth();
}

int RenderManager::getRenderWindowHeight()
{
   return viewport->getActualHeight();
}

void RenderManager::startRendering()
{
	ListArrayIterator<RenderListener>* iter = render_listeners->iterator();
	while(iter->hasNext())
	{
		RenderListener* render_listener = iter->next();
		render_listener->startRendering();
	}
	delete iter;
	root->startRendering();
}

void RenderManager::stopRendering()
{
	ListArrayIterator<RenderListener>* iter = render_listeners->iterator();
	while(iter->hasNext())
	{
		RenderListener* render_listener = iter->next();
		render_listener->stopRendering();
	}
	delete iter;
}

Ogre::RenderWindow* RenderManager::getRenderWindow()
{
   return window;
}

Ogre::SceneManager* RenderManager::getSceneManager()
{
   return scene_manager;
}

void RenderManager::addPathResource(string path, string level_name)
{
	Ogre::ResourceGroupManager& rgm = Ogre::ResourceGroupManager::getSingleton();
	rgm.addResourceLocation(path, "FileSystem", level_name);
}

void RenderManager::addMeshResource(string mesh, string level_name)
{
	Ogre::ResourceGroupManager& rgm = Ogre::ResourceGroupManager::getSingleton();
	rgm.declareResource(mesh, "Mesh", level_name);
}

void RenderManager::addAudioResource(string level_name, string file_name, string name, int type)
{
	AudioResource* whistle = new AudioResource(level_name, file_name, name, type);
	whistle->load(game_manager);
	audio_resources->add(whistle);
}

void RenderManager::unloadLevel(string level_name)
{
	scene_manager->clearScene();
	scene_manager->destroyAllCameras();
	window->removeAllViewports();
	
	Ogre::ResourceGroupManager& rgm = Ogre::ResourceGroupManager::getSingleton();
	rgm.destroyResourceGroup(level_name);
}

void RenderManager::loadLevel(string level_name)
{
	Ogre::ResourceGroupManager& rgm = Ogre::ResourceGroupManager::getSingleton();
	rgm.initialiseResourceGroup(level_name);
	game_manager->loadGUI(level_name);
	rgm.loadResourceGroup(level_name, true, true);
	scene_manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	scene_manager->setShadowColour( Ogre::ColourValue(0,0,0) );
}

void RenderManager::setupCamera(string cam_name, float* cam_position, float* cam_lookat, float* cam_clip)
{
	cam = scene_manager->createCamera(cam_name);
//	viewport = window->addViewport(cam, 0, 0.0, 0.0, 1.0, 1.0);
//	viewport->setBackgroundColour(Ogre::ColourValue(.47,.7,1));
	viewport->setCamera(cam);
	float width = Ogre::Real(viewport->getActualWidth());
	float height = Ogre::Real(viewport->getActualHeight());
	float aspect_ratio = width/height;
	
	cam->setAspectRatio(aspect_ratio);	
	cam->setPosition(cam_position[0], cam_position[1], cam_position[2]);
	cam->lookAt(cam_lookat[0], cam_lookat[1], cam_lookat[2]);
	cam->setNearClipDistance(cam_clip[0]);
	cam->setFarClipDistance(cam_clip[1]);
}

void RenderManager::setupLight(string light_name, string type, float* light_color, float* light_direction)
{
	scene_manager->setAmbientLight(Ogre::ColourValue(.05,.05,.05));
	Ogre::Light* light = scene_manager->createLight(light_name);
	if(type == "spot")
	{
		light->setType(Ogre::Light::LT_SPOTLIGHT);
	}
	else if(type == "point")
	{
		light->setType(Ogre::Light::LT_POINT);
	}
	else
	{
		light->setType(Ogre::Light::LT_DIRECTIONAL);
	}
	light->setDiffuseColour(light_color[0], light_color[1], light_color[2]);
	light->setDirection(Ogre::Vector3(light_direction[0], light_direction[1], light_direction[2]));
	if(light_name == "Light Top")
	{
		light->setCastShadows(true);
	}
}

void RenderManager::createEntity(std::string entity_name, std::string node_name, std::string mesh_name, std::string materials_name)
{
	Ogre::Entity* entity = scene_manager->createEntity(entity_name,mesh_name);
	entity->setMaterialName(materials_name);
	entity->setCastShadows(true);
	if(entity_name == "Plane") entity->setCastShadows(false);
	Ogre::SceneNode* node = scene_manager->getSceneNode(node_name);
	node->attachObject(entity);
}

void RenderManager::createSceneNode(std::string child_node, std::string parent_node)
{
	Ogre::SceneNode* root_node = scene_manager->getRootSceneNode();
	Ogre::SceneNode* new_node = scene_manager->createSceneNode(child_node);
	if(parent_node == "root")
	{
		root_node->addChild(new_node);
	}
	else
	{
		Ogre::SceneNode* parent = scene_manager->getSceneNode(parent_node);
		parent->addChild(new_node);
	}
}

void RenderManager::processTransformations(int transform_type, float* transform_values, string node_name)
{
	
	Ogre::SceneNode* to_transform = scene_manager->getSceneNode(node_name);
	
	if(transform_type == 1)
	{
		to_transform->translate(transform_values[0], transform_values[1], transform_values[2]);
		
	}
	else if(transform_type == 2)
	{
		Ogre::Degree deg(transform_values[0]);
		Ogre::Vector3 axis(transform_values[1], transform_values[2], transform_values[3]);
		Ogre::Quaternion quat(deg, axis);
		to_transform->rotate(quat);
		
	}
	else
	{
		to_transform->scale(transform_values[0], transform_values[1], transform_values[2]);
		
	}
}
void RenderManager::createAnimation(string animation_name, float animation_time, string animation_mode, string animation_node)
{
	Ogre::Animation* animation = scene_manager->createAnimation(animation_name, animation_time);
	if(animation_mode == "linear")
	{
		animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);
	}
	else
	{
		animation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
	}
	Ogre::SceneNode* scene_node = scene_manager->getSceneNode(animation_node);
	Ogre::NodeAnimationTrack* animation_track = animation->createNodeTrack(1, scene_node);
}

void RenderManager::createKeyFrame(string animation_name, float keyframe_time, float* key_translate, float* key_rotate, float* key_scale)
{
	Ogre::Animation* animation = scene_manager->getAnimation(animation_name);
	Ogre::NodeAnimationTrack* animation_track = animation->getNodeTrack(1);
	Ogre::TransformKeyFrame* keyframe = animation_track->createNodeKeyFrame(keyframe_time);
	
	keyframe->setTranslate(Ogre::Vector3(key_translate[0], key_translate[1], key_translate[2]));
	keyframe->setRotation(Ogre::Quaternion(Ogre::Degree(key_rotate[0]), Ogre::Vector3(key_rotate[1], key_rotate[2], key_rotate[3])));
	keyframe->setScale(Ogre::Vector3(key_scale[0], key_scale[1], key_scale[2]));
}

void RenderManager::createAnimationState(std::string animation_name)
{
	Ogre::AnimationState* animation_state = scene_manager->createAnimationState(animation_name);
	animation_states->add(animation_state);
}

void RenderManager::processAnimations(float time_step)
{
	ListArrayIterator<Ogre::AnimationState>* anim_iter = animation_states->iterator();
	while(anim_iter->hasNext())
	{
		Ogre::AnimationState* animation_state = anim_iter->next();
		animation_state->addTime(time_step);		
	}
	delete anim_iter;
}

void RenderManager::checkForInput(float time_step)
{
	game_manager->checkForInput(time_step);
}

void RenderManager::playAudio(string audio_name)
{
	game_manager->playAudio(audio_name);	
}

void RenderManager::guiButtonCallback(string combo_box_string, string script_name)
{
	script_manager->guiButtonCallback(combo_box_string, script_name);
}

ListArray<AudioResource>* RenderManager::getAudioResources()
{
	return audio_resources;
}

void RenderManager::resetMeshes()
{
	Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
	scene_node->setOrientation(Ogre::Quaternion(Ogre::Degree(-360),Ogre::Vector3(0,1,0)));
	scene_node = scene_manager->getSceneNode("Torus Transform Node");
	scene_node->setPosition(Ogre::Vector3(9,5,0));
}

void RenderManager::updateAudio(float time_step)
{
	game_manager->updateAudio(time_step);
}

void RenderManager::keyPressed(string keyMap)
{
	if(keyMap == "ESCAPE")
	{
		stopRendering();
	}
	else if(keyMap == "A" || keyMap == "D")
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion current_quat = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		current_quat.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		if(keyMap == "A")
		{
			curr_deg = prev_degree.valueDegrees() + 5;
		}
		if(keyMap == "D")
		{
			curr_deg = prev_degree.valueDegrees() - 5;
		}
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
}

void RenderManager::mousePressed(uint32 x_click, uint32 y_click, string mouseMap)
{
	if(mouseMap == "left")
	{
		
	}
}

void RenderManager::buttonPressed(std::string buttonMap)
{
	if(buttonMap == "RJS")
	{
		
		game_manager->playAudio("whistle");
		
		

		animation_states->remove(1);
		//animation_states->remove(1);
		scene_manager->destroyAnimationState("Torus Animation");
		//scene_manager->destroyAnimationState("Sword Animation");		
		Ogre::AnimationState* torus_animation = scene_manager->createAnimationState("Torus Animation");
		//Ogre::AnimationState* sword_animation = scene_manager->createAnimationState("Sword Animation");
		torus_animation->setEnabled(true);
		torus_animation->setLoop(false);
		//sword_animation->setEnabled(true);
		//sword_animation->setLoop(false);
		//animation_states->add(sword_animation);
		animation_states->add(torus_animation);
		
		
	}
	if(buttonMap == "B")
	{
		
	}
}

void RenderManager::rightJoystickMoved(float ns, float ew)
{
	
	if(ns > 10000 && ns < 15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.001,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -10000 && ns > -15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.001,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -15000 && ns > -20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.002,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 15000 && ns < 20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.002,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -20000 && ns > -25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.004,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 20000 && ns < 25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.004,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -25000 && ns > -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.007,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 25000 && ns < 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.007,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.011,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.011,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	
	if(ew > 10000 && ew < 15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(.001,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew < -10000 && ew > -15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(-.001,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew < -15000 && ew > -20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(-.002,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew > 15000 && ew < 20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(.002,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew < -20000 && ew > -25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(-.004,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew > 20000 && ew < 25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(.004,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew < -25000 && ew > -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(-.007,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew > 25000 && ew < 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(.007,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew < -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(-.011,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ew > 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Torus Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(.011,0,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	
}

void RenderManager::leftJoystickMoved(float ns, float ew)
{
	/*
	if(ns > 10000 && ns < 15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		current_quat.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() * 1.05;
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ns < -10000 && ns > -15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.001,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -15000 && ns > -20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.002,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 15000 && ns < 20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.002,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -20000 && ns > -25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.004,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 20000 && ns < 25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.004,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -25000 && ns > -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.007,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 25000 && ns < 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.007,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns < -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,.011,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	else if(ns > 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Transform Node");
		Ogre::Vector3 curr_position = scene_node->getPosition();
		Ogre::Vector3 add_y(0,-.011,0);
		curr_position = curr_position + add_y;
		scene_node->setPosition(curr_position);
	}
	*/
	if(ew > 10000 && ew < 15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() - .01;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	
	else if(ew < -10000 && ew > -15000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() + .01;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew < -15000 && ew > -20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() + .02;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew > 15000 && ew < 20000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() - .02;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew < -20000 && ew > -25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() + .04;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew > 20000 && ew < 25000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() - .04;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew < -25000 && ew > -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() + .07;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew > 25000 && ew < 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() - .07;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew < -30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() + .11;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	else if(ew > 30000)
	{
		Ogre::SceneNode* scene_node = scene_manager->getSceneNode("Sword Animation Node");
		Ogre::Quaternion curr_position = scene_node->getOrientation();
		Ogre::Degree prev_degree;
		Ogre::Vector3 axis;
		curr_position.ToAngleAxis(prev_degree, axis);
		float curr_deg;
		curr_deg = prev_degree.valueDegrees() - .11;
		if(curr_deg < 0)
		{
			curr_deg += 360;
		}
		if(curr_deg > 360)
		{
			curr_deg -= 360;
		}
		Ogre::Degree curr_degree(curr_deg);
		Ogre::Quaternion new_quat(curr_degree, axis);
		scene_node->setOrientation(new_quat);
	}
	
}

/*
void RenderManager::parseResources(const char* file_name)
{
	Ogre::ResourceGroupManager& rgm = Ogre::ResourceGroupManager::getSingleton();
	
	TiXmlDocument resources(file_name);
	if(!resources.LoadFile()) return;
	
	TiXmlElement* levels_element = resources.RootElement();
	TiXmlElement* level_element = levels_element->FirstChildElement("level");
	
	while(level_element != NULL)
	{
		TiXmlElement* level_name_element = level_element->FirstChildElement("name");
		string level_name = level_name_element->GetText();
		
		TiXmlElement* paths_element = level_element->FirstChildElement("paths");
		TiXmlElement* path_element = paths_element->FirstChildElement("path");
		
		while(path_element != NULL)
		{
			string path = path_element->GetText();
			rgm.addResourceLocation(path,"FileSystem",level_name);
			path_element = path_element->NextSiblingElement();
		}
		
		TiXmlElement* meshes_element = level_element->FirstChildElement("meshes");
		TiXmlElement* mesh_element = meshes_element->FirstChildElement("mesh");
		
		while(mesh_element != NULL)
		{
			string mesh = mesh_element->GetText();
			rgm.declareResource(mesh,"Mesh",level_name);
			mesh_element = mesh_element->NextSiblingElement();
		}		
		
		rgm.initialiseResourceGroup(level_name);
		rgm.loadResourceGroup(level_name, true, true);
		
		level_element = level_element->NextSiblingElement();
		
	}
	
}

void RenderManager::parseScene(const char* filename)
{
	
	
	TiXmlDocument scene(filename);
	if(!scene.LoadFile()) return;
	
	TiXmlElement* levels_element = scene.RootElement();
	TiXmlElement* level_element = levels_element->FirstChildElement("level");
	
	while(level_element != NULL)
	{
		TiXmlElement* level_name = level_element->FirstChildElement("name");
		
		TiXmlElement* camera_element = level_element->FirstChildElement("camera");
		TiXmlElement* camera_name_element = camera_element->FirstChildElement("name");
		string camera_name = camera_name_element->GetText();
		cam = scene_manager->createCamera(camera_name);
		viewport = window->addViewport(cam, 0, 0.0, 0.0, 1.0, 1.0);
		viewport->setBackgroundColour(Ogre::ColourValue(0,0,0));
		float actual_width = Ogre::Real(viewport->getActualWidth());
		float actual_height = Ogre::Real(viewport->getActualHeight());
		float aspect_ratio = actual_width/actual_height;
		
		cam->setAspectRatio(aspect_ratio);	
		cam->setPosition(Ogre::Vector3(5,3,15));
		cam->lookAt(Ogre::Vector3(0,0,-1000));
		cam->setNearClipDistance(2);
		cam->setFarClipDistance(50);
		
		scene_manager->setAmbientLight(Ogre::ColourValue(.05,.05,.05));
		Ogre::Light* light = scene_manager->createLight("Light");
		light->setType(Ogre::Light::LT_DIRECTIONAL);	
		light->setDiffuseColour(1.0,1.0,1.0);
		light->setDirection(0.0,0,-1.0);
		
		TiXmlElement* camera_position_element = camera_element->FirstChildElement("position");
		
		TiXmlElement* camera_lookat_element = camera_element->FirstChildElement("lookat");
		TiXmlElement* camera_clip_element = camera_element->FirstChildElement("clip");
		
		TiXmlElement* light_element = level_element->FirstChildElement("light");
		TiXmlElement* light_name_element = light_element->FirstChildElement("name");
		TiXmlElement* light_type_element = light_element->FirstChildElement("type");
		TiXmlElement* light_color_element = light_element->FirstChildElement("color");
		TiXmlElement* light_direction_element = light_element->FirstChildElement("direction");
		
		TiXmlElement* graph_element = level_element->FirstChildElement("graph");
		TiXmlElement* children_element = graph_element->FirstChildElement("children");
		Ogre::SceneNode* root_node = scene_manager->getRootSceneNode();
		if(children_element != NULL)
		{
			processChild(children_element, root_node);
		}
		
		level_element = level_element->NextSiblingElement();
	}
}

void RenderManager::processChild(TiXmlElement* children_element, Ogre::SceneNode* parent_node)
{
	TiXmlElement* child_element = children_element->FirstChildElement("child");
	
	while(child_element != NULL)
	{
		TiXmlElement* node_name_element = child_element->FirstChildElement("name");
		string node_name = node_name_element->GetText();
		Ogre::SceneNode* scene_node = scene_manager->createSceneNode(node_name);
		parent_node->addChild(scene_node);
		
		TiXmlElement* entity_element = child_element->FirstChildElement("entity");
		if(entity_element != NULL)
		{
			TiXmlElement* entity_name_element = entity_element->FirstChildElement("name");
			string entity_name = entity_name_element->GetText();
			TiXmlElement* entity_mesh_element = entity_element->FirstChildElement("mesh");
			string mesh_name = entity_mesh_element->GetText();
			TiXmlElement* entity_materials_element = entity_element->FirstChildElement("materials");
			string materials_name = entity_materials_element->GetText();
			
			Ogre::Entity* entity = scene_manager->createEntity(entity_name, mesh_name);
			entity->setMaterialName(materials_name);
			scene_node->attachObject(entity);
		}
		
		TiXmlElement* animation_element = child_element->FirstChildElement("animation");
		if(animation_element != NULL)
		{
			processAnimation(animation_element, scene_node);
		}
		
		TiXmlElement* translate_element = child_element->FirstChildElement("translation");
		string translate = translate_element->GetText();
		processTransformations(1, translate, scene_node);
		TiXmlElement* rotation_element = child_element->FirstChildElement("rotation");
		string rotate = rotation_element->GetText();
		processTransformations(2, rotate, scene_node);
		TiXmlElement* scale_element = child_element->FirstChildElement("scale");
		string scale = scale_element->GetText();
		processTransformations(3, scale, scene_node);
		TiXmlElement* grand_children_element = child_element->FirstChildElement("children");
		if(grand_children_element != NULL)
		{
			processChild(grand_children_element, scene_node);
		}
		child_element = child_element->NextSiblingElement();
	}
}

void RenderManager::processAnimation(TiXmlElement* animation_element, Ogre::SceneNode* animation_node)
{
	
	TiXmlElement* animation_name_element = animation_element->FirstChildElement("name");
	string animation_name = animation_name_element->GetText();
	TiXmlElement* animation_mode_element = animation_element->FirstChildElement("mode");
	string animation_mode = animation_mode_element->GetText();
	TiXmlElement* animation_time_element = animation_element->FirstChildElement("seconds");
	string animation_time_str = animation_time_element->GetText();
	float animation_time = Utils::parseFloat(animation_time_str);
	Ogre::Animation* myAnimation = scene_manager->createAnimation(animation_name, animation_time);
	if(animation_mode == "linear")
	{
		myAnimation->setInterpolationMode(Ogre::Animation::IM_LINEAR);
	}
	else
	{
		myAnimation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
	}
	Ogre::NodeAnimationTrack* animation_track = myAnimation->createNodeTrack(1, animation_node);
	Ogre::TransformKeyFrame* keyframe;
	
	TiXmlElement* animation_keyframes_element = animation_element->FirstChildElement("keyframes");
	TiXmlElement* animation_keyframe_element = animation_keyframes_element->FirstChildElement("keyframe");
	float keyframe_time = 0;
	while(animation_keyframe_element != NULL)
	{
		TiXmlElement* keyframe_time_element = animation_keyframe_element->FirstChildElement("time");
		string keyframe_time_str = keyframe_time_element->GetText();
		keyframe_time = Utils::parseFloat(keyframe_time_str);
		keyframe = animation_track->createNodeKeyFrame(keyframe_time);
		
		TiXmlElement* keyframe_trans_element = animation_keyframe_element->FirstChildElement("translation");
		string keyframe_trans_str = keyframe_trans_element->GetText();
		float* key_translate = new float[3];
		Utils::parseFloats(keyframe_trans_str, key_translate);		
		keyframe->setTranslate(Ogre::Vector3(key_translate[0], key_translate[1], key_translate[2]));
		delete key_translate;
		
		TiXmlElement* keyframe_rot_element = animation_keyframe_element->FirstChildElement("rotation");
		string keyframe_rot_str = keyframe_rot_element->GetText();
		float* key_rotate = new float[4];
		Utils::parseFloats(keyframe_rot_str, key_rotate);
		keyframe->setRotation(Ogre::Quaternion(Ogre::Degree(key_rotate[0]), Ogre::Vector3(key_rotate[1], key_rotate[2], key_rotate[3])));
		delete key_rotate;
		
		TiXmlElement* keyframe_scale_element = animation_keyframe_element->FirstChildElement("scale");
		string keyframe_scale_str = keyframe_scale_element->GetText();
		float* key_scale = new float[3];
		Utils::parseFloats(keyframe_scale_str, key_scale);		
		keyframe->setScale(Ogre::Vector3(key_scale[0], key_scale[1], key_scale[2]));
		delete key_scale;
		
		animation_keyframe_element = animation_keyframe_element->NextSiblingElement();
	}
	
	Ogre::AnimationState* myAnimationState = scene_manager->createAnimationState(animation_name);
	myAnimationState->setEnabled(true);
	myAnimationState->setLoop(true);
	
	animation_states->add(myAnimationState);
	
}

void RenderManager::processTransformations(int transform_type, string& transform_str, Ogre::SceneNode* scene_node)
{
	float* transform_values = new float[4];
	Utils::parseFloats(transform_str, transform_values);
	
	if(transform_type == 1)
	{
		scene_node->translate(transform_values[0], transform_values[1], transform_values[2]);
		delete transform_values;
	}
	else if(transform_type == 2)
	{
		Ogre::Degree deg(transform_values[0]);
		Ogre::Vector3 axis(transform_values[1], transform_values[2], transform_values[3]);
		Ogre::Quaternion quat(deg, axis);
		scene_node->rotate(quat);
		delete transform_values;
	}
	else
	{
		scene_node->scale(transform_values[0], transform_values[1], transform_values[2]);
		delete transform_values;
	}
}

void RenderManager::buildAnimation(Ogre::SceneNode* sword_animation_node, Ogre::SceneNode* torus_animation_node)
{
	
	Ogre::Vector3 x_axis(1,0,0);
	Ogre::Vector3 y_axis(0,1,0);
	Ogre::Vector3 z_axis(0,0,1);
	Ogre::Vector3 full(1,1,1);
	Ogre::Vector3 half(.5,.5,.5);
	Ogre::Vector3 quarter(.25,.25,.25);
	
	Ogre::Animation* myAnimation = scene_manager->createAnimation("My Animation", 9.0);
	myAnimation->setInterpolationMode(Ogre::Animation::IM_LINEAR);
	
	Ogre::NodeAnimationTrack* sword_track = myAnimation->createNodeTrack(1, sword_animation_node);
	Ogre::NodeAnimationTrack* torus_track = myAnimation->createNodeTrack(2, torus_animation_node);
	
	Ogre::TransformKeyFrame* sword_key = sword_track->createNodeKeyFrame(0);
	Ogre::TransformKeyFrame* torus_key = torus_track->createNodeKeyFrame(0);
	Ogre::Quaternion q1(Ogre::Degree(0),y_axis);
	sword_key->setRotation(q1);
	torus_key->setTranslate(Ogre::Vector3(0,0,0));
	
	torus_key = torus_track->createNodeKeyFrame(2.0);
	Ogre::Quaternion q2(Ogre::Degree(90),z_axis);
	torus_key->setRotation(q2);
	torus_key->setScale(quarter);
	torus_key->setTranslate(Ogre::Vector3(-4.15,8.7,0.0));
	
	sword_key = sword_track->createNodeKeyFrame(4.0);
	torus_key = torus_track->createNodeKeyFrame(4.0);
	Ogre::Quaternion q3(Ogre::Degree(80),y_axis);
	sword_key->setRotation(q3);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(180),z_axis));
	torus_key->setTranslate(Ogre::Vector3(-8.3,4.0,0));
	torus_key->setScale(full);	
	
	sword_key = sword_track->createNodeKeyFrame(4.5);
	torus_key = torus_track->createNodeKeyFrame(4.5);
	sword_key->setRotation(q3);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(180),z_axis));
	torus_key->setTranslate(Ogre::Vector3(-8.6,2.0,0));
	
	sword_key = sword_track->createNodeKeyFrame(5.0);
	torus_key = torus_track->createNodeKeyFrame(5.0);
	sword_key->setRotation(q3);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(180),z_axis));
	torus_key->setTranslate(Ogre::Vector3(-9,0,0));
	
	sword_key = sword_track->createNodeKeyFrame(6.0);
	torus_key = torus_track->createNodeKeyFrame(6.0);
	sword_key->setRotation(q3);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(180),z_axis));
	torus_key->setTranslate(Ogre::Vector3(-9,0,0));
	
	torus_key = torus_track->createNodeKeyFrame(6.2);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(180),z_axis));
	torus_key->setTranslate(Ogre::Vector3(-7.7,2.3,0));
	
	sword_key = sword_track->createNodeKeyFrame(6.4);
	torus_key = torus_track->createNodeKeyFrame(6.4);
	Ogre::Quaternion q4(Ogre::Degree(35),y_axis);
	sword_key->setRotation(q4);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(180),z_axis));
	torus_key->setTranslate(Ogre::Vector3(-5.6,2.9,0));
	
	sword_key = sword_track->createNodeKeyFrame(6.5);
	sword_key->setRotation(q1);
	
	sword_key = sword_track->createNodeKeyFrame(7.0);
	torus_key = torus_track->createNodeKeyFrame(7.0);
	sword_key->setRotation(q1);
	torus_key->setRotation(Ogre::Quaternion(Ogre::Degree(0),z_axis));
	torus_key->setTranslate(Ogre::Vector3(5,5,0));
	sword_key = sword_track->createNodeKeyFrame(8.0);
	torus_key = torus_track->createNodeKeyFrame(8.0);
	sword_key->setRotation(q1);
	torus_key->setTranslate(Ogre::Vector3(0,0,0));
	
	
	
	
	Ogre::AnimationState* myAnimationState = scene_manager->createAnimationState("My Animation");
	myAnimationState->setEnabled(true);
	myAnimationState->setLoop(true);
	
	animation_states->add(myAnimationState);
	
	
}

void RenderManager::buildSimpleScene()
{
	
	Ogre::ResourceGroupManager& rgm = Ogre::ResourceGroupManager::getSingleton();
	rgm.addResourceLocation("./assets/models","FileSystem","Level_1");
	rgm.addResourceLocation("./assets/materials/scripts","FileSystem","Level_1");
	//rgm.declareResource("Sphere.mesh","Mesh","Level_1");
	rgm.declareResource("Torus.mesh","Mesh","Level_1");
	rgm.declareResource("ShortSword.mesh","Mesh","Level_1");
	rgm.initialiseResourceGroup("Level_1");
	rgm.loadResourceGroup("Level_1",true,true);
	
	cam = scene_manager->createCamera("Camera");
	  
    //z-order, left, top, width, height
    viewport = window->addViewport(cam, 0, 0.0, 0.0, 1.0, 1.0);  //assign a camera to a viewport (can have many cameras and viewports in a single window)
    viewport->setBackgroundColour(Ogre::ColourValue(0,0,0));
    float actual_width = Ogre::Real(viewport->getActualWidth());
    float actual_height = Ogre::Real(viewport->getActualHeight());
    float aspect_ratio = actual_width/actual_height;
	
    cam->setAspectRatio(aspect_ratio);	
	cam->setPosition(Ogre::Vector3(5,3,15));
	cam->lookAt(Ogre::Vector3(0,0,-1000));
	cam->setNearClipDistance(2);
	cam->setFarClipDistance(50);
	
	scene_manager->setAmbientLight(Ogre::ColourValue(.05,.05,.05));
	Ogre::Light* light = scene_manager->createLight("Light");
	light->setType(Ogre::Light::LT_DIRECTIONAL);	
	light->setDiffuseColour(1.0,1.0,1.0);
	light->setDirection(0.0,0,-1.0);
	
	Ogre::SceneNode* scene_root_node = scene_manager->getRootSceneNode();
	Ogre::SceneNode* torus_node = scene_manager->createSceneNode("Torus_Node");
	Ogre::SceneNode* sword_node = scene_manager->createSceneNode("Sword_Node");
	Ogre::SceneNode* sword_tnode = scene_manager->createSceneNode("Sword_Transform_Node");
	Ogre::SceneNode* torus_tnode = scene_manager->createSceneNode("Torus_Transform_Node");
	Ogre::SceneNode* sword_anode = scene_manager->createSceneNode("Sword_Animation_Node");
	Ogre::SceneNode* torus_anode = scene_manager->createSceneNode("Torus_Animation_Node");
	
	Ogre::Vector3 v0(0,0,0);
	Ogre::Quaternion q0(1,0,0,0);
	torus_tnode->translate(9,0,0);
	torus_node->scale(.5,.5,.5);
	sword_node->scale(.1,.1,.1);
	Ogre::Quaternion qq(Ogre::Degree(90),Ogre::Vector3(1,0,0));
	Ogre::Quaternion qw(Ogre::Degree(-90),Ogre::Vector3(0,1,0));
	sword_tnode->rotate(qq);
	sword_tnode->rotate(qw);
	Ogre::Quaternion qm = qq * qw;
	Ogre::Real one = qm[0];
	Ogre::Real two = qm[1];
	Ogre::Real three = qm[2];
	Ogre::Real four = qm[3];
	
	float print1 = *((float *) &one);
	float print2 = *((float *) &two);
	float print3 = *((float *) &three);
	float print4 = *((float *) &four);
	
	cout << "( " << print1 << ", " << print2 << ", " << print3 << ", " << print4 << ")" << endl;
	
	buildAnimation(sword_anode, torus_anode);
	
	Ogre::Entity* torus_entity = scene_manager->createEntity("Torus_Entity","Torus.mesh");	
	torus_entity->setMaterialName("Torus");
	Ogre::Entity* sword_entity = scene_manager->createEntity("Sword_Entity","ShortSword.mesh");
	sword_entity->setMaterialName("ShortSword");	
	
	scene_root_node->addChild(sword_tnode);
	sword_tnode->addChild(sword_anode);
	sword_anode->addChild(sword_node);	
	sword_node->attachObject(sword_entity);
	
	scene_root_node->addChild(torus_tnode);
	torus_tnode->addChild(torus_anode);
	torus_anode->addChild(torus_node);	
	torus_node->attachObject(torus_entity);
	
	
}
*/





















