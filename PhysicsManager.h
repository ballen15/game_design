#if !defined PHYSICS_MANAGER
#define PHYSICS_MANAGER

#include "btbulletDynamicsCommon.h"
#include "CSC2110/TableAVL.h"

class RigidBody;
class CompoundShape;
class RenderManager;
struct SceneNodeMotion;

class PhysicsManager
{
	private:
		RenderManager* render_manager;
		
	
	public:
		PhysicsManager(RenderManager* rm);
		virtual ~PhysicsManager();
		void stepPhysics(float time_inc);
		void updateRigidBodyVelocity(float time_inc);
		void setGravity(float* gravity);
		void getGravity(float* gravity);
		void getLinearVelocity(std::string rigid_body, float* velocity);
		void setRigidBodyVelocity(std::string rigid_body, float end_time, float* acceleration);
		void init();
		void createRigidBodies();
		void createCollisionShape(std::string compound_shape);
		
};

#endif