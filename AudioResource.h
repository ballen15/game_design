#if !defined AUDIO_RESOURCE
#define AUDIO_RESOURCE

#include "GameResource.h"

class GameManager;
struct AR_Info;

class AudioResource : public GameResource
{
	private:
		AR_Info* arinfo;
		std::string name;
		
		//1 for sample, 2 for stream
		int type;		
	
	public:
		AudioResource(std::string level_name, std::string file_name, std::string audio_name, int audio_type);
		virtual ~AudioResource();
		
		virtual void load(GameManager* gm);
		virtual void unload(GameManager* gm);
		
		AR_Info* getAudioInfo();
		std::string getName();
		int getType();
		

};

#endif