#include "AudioManager.h"
#include "AudioResource.h"
#include "GameManager.h"
#include "CSC2110/ListArray.h"
#include "CSC2110/ListArrayIterator.h"
#include <iostream>

struct AR_Info
{
	HSAMPLE sample_data;
	HSTREAM stream_data;
	HSAMPLE channel_data;
};

AudioManager::AudioManager(GameManager* gm)
{
	game_manager = gm;
	init();
}

AudioManager::~AudioManager()
{
	
}

AR_Info* AudioManager::createAudioInfo()
{
	AR_Info* arinfo = (AR_Info*) malloc(sizeof(AR_Info));
	arinfo->sample_data = 0;
	arinfo->stream_data = 0;
	arinfo->channel_data = 0;
	return arinfo;
}

void AudioManager::init(int Device, DWORD SampleRate, DWORD flags, HWND win)
{
	BOOL bassActive = BASS_Init(Device, SampleRate, flags, win, NULL);
	if(!bassActive)
	{
		ASSERT_LOG(false, "Audio Manager Init Problem");
	}
	
	std::stringstream DeviceStringStream;
	if(BASS_GetDeviceInfo, &device_info)
	{
		game_manager->logComment("Got Device Info With Bass");
	}
	else
	{
		BASS_Free();
		ASSERT_LOG(false, "Audio Manager Init Problem");
	}
}

void AudioManager::free()
{
	BASS_Free();
}

void AudioManager::updateAudio(float time_step)
{
	/*
	if(BASS_ChannelIsActive(arinfo->channel_data) == BASS_ACTIVE_STOPPED)
	{
		resource = NULL;
	}
	*/
}

void AudioManager::playAudio(std::string audio_name)
{
	if(audio_name != "")
	{
		AR_Info* arinfo_new = NULL;
		AR_Info* arinfo_playing = NULL;
		bool playing = false;
		ListArray<AudioResource>* audio_resources = game_manager->getAudioResources();
		ListArrayIterator<AudioResource>* audio_iter = audio_resources->iterator();
		while(audio_iter->hasNext())
		{
			AudioResource* ar = audio_iter->next();
			if(ar->getAudioInfo()->stream_data && BASS_ChannelIsActive(ar->getAudioInfo()->channel_data) == BASS_ACTIVE_PLAYING)
			{
				arinfo_playing = ar->getAudioInfo();
				playing = true;
			}
			if(ar->getName() == audio_name)
			{
				arinfo_new = ar->getAudioInfo();
			}
		}
		if(arinfo_new->stream_data && playing)
		{
			
			BASS_ChannelStop(arinfo_playing->channel_data);
			BASS_ChannelSetPosition(arinfo_playing->channel_data, 0, 0);
		}
		
		BASS_ChannelPlay(arinfo_new->channel_data, false);
		
		/*
		if(arinfo->sample_data)
		{
			BASS_ChannelPlay(arinfo->channel_data, false);
		}
		else
		{
			BASS_ChannelStop(arinfo->channel_data);
			BASS_ChannelPlay(arinfo->channel_data, false);
		}
		*/
	}
	
}

void AudioManager::loadAudio(std::string file_name, AR_Info* arinfo, int type)
{
	if(type == 1)
	{
		arinfo->sample_data = BASS_SampleLoad(FALSE, file_name.c_str(),0,0,1,0);
	}
	else
	{
		arinfo->stream_data = BASS_StreamCreateFile(FALSE, file_name.c_str(),0,0,0);
	}
	if(arinfo->stream_data)
	{
		arinfo->channel_data = arinfo->stream_data;
	}
	else if(arinfo->sample_data)
	{
		arinfo->channel_data = BASS_SampleGetChannel(arinfo->sample_data, false);
	}
	else
	{
		ASSERT_LOG(false, "Audio Resource Initialization error" + file_name);
	}
}

void AudioManager::unloadAudio(AR_Info* arinfo, int type)
{
	if(type == 1)
	{
		if(!BASS_ChannelIsActive(arinfo->channel_data) == BASS_ACTIVE_STOPPED)
		{
			BASS_ChannelStop(arinfo->channel_data);
		}
		
		BASS_SampleFree(arinfo->sample_data);
		arinfo->sample_data = 0;
		arinfo->channel_data = 0;
	}
	else
	{
		if(!BASS_ChannelIsActive(arinfo->channel_data) == BASS_ACTIVE_STOPPED)
		{
			BASS_ChannelStop(arinfo->channel_data);
		}
		
		BASS_StreamFree(arinfo->stream_data);
		arinfo->stream_data = 0;
		arinfo->channel_data = 0;  
	}
}












 