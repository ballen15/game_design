#include "ResourceManager.h"
#include "GameManager.h"
#include "GameXMLResources.h"
#include "CSC2110/ListArrayIterator.h"

#include <iostream>
using namespace std; 

ResourceManager::ResourceManager(const char* resources_file, const char* scene_file, GameManager* gm)
{
	loaded_level_name = "";
	game_manager = gm;
	
	try
	{
		game_xml = new GameXMLResources(resources_file, scene_file);
	}
	catch(exception& e)
	{
		ASSERT_CRITICAL(false, e.what());
	}
	
	
}

ResourceManager::~ResourceManager()
{
	delete game_xml;
}

void ResourceManager::unloadLevel()
{
	game_manager->unloadLevel(loaded_level_name);
	loaded_level_name = "";
}

void ResourceManager::loadLevel(string level_to_load)
{
	game_resources_by_level = game_xml->getResourcesByLevel(level_to_load, game_manager);
	game_manager->loadLevel(level_to_load);
	game_xml->buildXMLScene(level_to_load, game_manager);
	
}

void ResourceManager::addPathResource(string path, string level_name)
{
	
}

void ResourceManager::addMeshResource(string mesh, string level_name)
{
	
}







