#if !defined RESOURCE_MANAGER
#define RESOURCE_MANAGER

#include<string>
#include "CSC2110/ListArray.h"
using namespace std;

class GameManager;
class GameXMLResources;
class GameResource;

class ResourceManager
{
	private:
		GameManager* game_manager;
		GameXMLResources* game_xml;
		
		ListArray<GameResource>* game_resources_by_level;
		
		string loaded_level_name;
		
	public:
		ResourceManager(const char* resources_file, const char* scene_file, GameManager* gm);
		virtual ~ResourceManager();
		
		void unloadLevel();
		void loadLevel(string level_to_load);
		
		void addPathResource(string path, string level_name);
		void addMeshResource(string mesh, string level_name);

};

#endif