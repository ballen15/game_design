#include "GameResource.h"

GameResource::GameResource(string level_name, string file_name, GameResourceType type)
{
	resource_level = level_name;
	resource_file = file_name;
	resource_type = type;
	is_loaded = false;
}

GameResource::~GameResource()
{
}

bool GameResource::isLoaded()
{
	return is_loaded;
}

string GameResource::getLevelName()
{
	return resource_level;
}

string GameResource::getFileName()
{
	return resource_file;
}

void GameResource::load()
{
	
}

void GameResource::unload()
{
	
}

GameResourceType GameResource::getResourceType()
{
	return resource_type;
}