#include "LogManager.h"

using namespace std;

LogManager::LogManager(std::string file_name)
{
   create(file_name);
}

LogManager::~LogManager()
{
   close();
}

void LogManager::create(std::string file_name)
{
   log_file.open(file_name.c_str());
}

void LogManager::flush()
{
   log_file << log_buffer.str();
   log_file.flush();
   log_buffer.str("");
}

void LogManager::close()
{
   log_file.close();
}

void LogManager::logComment(std::string comment_message)
{
   log_buffer << "****COMMENT****" << endl;
   flush();

   log_buffer << getTimeString() << endl << comment_message << endl;
   flush();

   log_buffer << "***************" << endl;
   flush();
}

void LogManager::logProblem(std::string message, std::string src_file_name, int line_number)
{
   log_buffer << "****ASSERTION FAILURE****" << endl;
   flush();
   
   log_buffer << "MESSAGE: " << message << endl;
   flush();
   
   stringstream error_str;
   error_str << "Assertion Failed: " << endl << "Src File: " << src_file_name << endl << "Line Number: " << line_number << endl;
   string error_text = error_str.str();

   log_buffer << getTimeString() << endl << error_text;
   flush();

   log_buffer << "*************************\r\n";
   flush();
}

std::string LogManager::getTimeString()
{
   std::stringstream time_str;

   struct tm* pTime;
   time_t ctTime; 
   time(&ctTime);
   pTime = localtime(&ctTime);

   time_str << std::setw(2) << std::setfill('0') << pTime->tm_hour << ":";
   time_str << std::setw(2) << std::setfill('0') << pTime->tm_min << ":";
   time_str << std::setw(2) << std::setfill('0') << pTime->tm_sec;

   return time_str.str();
}
