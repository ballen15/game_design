#if !defined GAME_XML_RESOURCES
#define GAME_XML_RESOURCES

#include "tinyxml.h"
#include "CSC2110/ListArray.h"
#include "GameResource.h"

class GameManager;

class GameXMLResources
{
	private:
		TiXmlDocument resources_document;
		TiXmlDocument scenes_document;
		
		void processChildren(TiXmlElement* children_element, std::string parent_name, GameManager* game_manager);
		void processAnimation(TiXmlElement* animation_element, std::string animation_node, GameManager* game_manager);
	
	public:
		GameXMLResources(const char* resources_filename, const char* scene_filename);
		virtual ~GameXMLResources();
		
		ListArray<GameResource>* getResourcesByLevel(std::string level_name, GameManager* game_manager);
		void buildXMLScene(std::string requested_level, GameManager* game_manager);

};

#endif