#if !defined SCRIPT_MANAGER
#define SCRIPT_MANAGER

extern "C"
{
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}

#include <LuaBridge.h>
#include <string>
class GameManager;

class ScriptManager
{
	
	private:
		lua_State* L;
		GameManager* game_manager;

	public:
		ScriptManager(GameManager* gm);
		virtual ~ScriptManager();
		void guiButtonCallback(std::string combo_box_text, std::string event_script);
		void leftJoystickCallback(float ns, float ew, std::string event_script);

};

#endif