#include "PathResource.h"
#include "GameManager.h"

PathResource::PathResource(string level_name, string path, GameResourceType type, GameManager* gm) : GameResource(level_name, path, type)
{
	game_manager = gm;
	
}

PathResource::~PathResource()
{
	game_manager = NULL;
}

void PathResource::load()
{
	game_manager->addPathResource(getFileName(), getLevelName());
	is_loaded = true;
}

void PathResource::unload()
{
	is_loaded = false;
}