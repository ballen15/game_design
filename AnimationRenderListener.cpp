#include "AnimationRenderListener.h"
#include "RenderManager.h"

#include <iostream>
using namespace std;

AnimationRenderListener::AnimationRenderListener(RenderManager* rm) : RenderListener(rm)
{
}

AnimationRenderListener::~AnimationRenderListener()
{
}

bool AnimationRenderListener::frameStarted(const Ogre::FrameEvent& event)
{
	float time_step = event.timeSinceLastFrame;
	getRenderManager()->processAnimations(time_step);
	return getRenderStatus();
}




















