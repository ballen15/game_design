#include "GUIManager.h"
#include "RenderManager.h"
#include <iostream>


GUIManager::GUIManager(RenderManager* rm)
{
	render_manager = rm;
	gui_context = NULL;
	cegui_renderer = getRenderer();
	root_window = NULL;
}

GUIManager::~GUIManager()
{
	cegui_renderer = NULL;
}

void GUIManager::createContext()
{
	CEGUI::SchemeManager::getSingleton().createFromFile("VanillaSkin.scheme");
	CEGUI::FontManager::getSingleton().createFromFile("mizufalp.font");
	CEGUI::System& system = CEGUI::System::getSingleton();
	gui_context = &system.createGUIContext(cegui_renderer->getDefaultRenderTarget());
	gui_context->setDefaultFont("mizufalp");
	gui_context->getMouseCursor().setDefaultImage("Vanilla-Images/MouseArrow");
	gui_context->setDefaultTooltipType("Vanilla/Tooltip");
	
	CEGUI::WindowManager* wmgr = &CEGUI::WindowManager::getSingleton();
	root_window = wmgr->loadLayoutFromFile("MyLayout.layout");
	gui_context->setRootWindow(root_window);
	
	createComboBox();
	
	CEGUI::PushButton* play_button = static_cast<CEGUI::PushButton*>(root_window->getChild("FrameWindow/PlayButton"));
	play_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GUIManager::buttonEvent, this));
	
	CEGUI::PushButton* reset_button = static_cast<CEGUI::PushButton*>(root_window->getChild("FrameWindow/ResetButton"));
	reset_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GUIManager::resetEvent, this));
	
}

void GUIManager::createComboBox()
{
	CEGUI::Combobox* combo_box = static_cast<CEGUI::Combobox*>(root_window->getChild("FrameWindow/Combobox"));
	combo_box->setReadOnly(true);
	
	CEGUI::ListboxTextItem* item1 = new CEGUI::ListboxTextItem("Blink-182: Adam's Song", 1);
	item1->setSelectionBrushImage("Vanilla-Images/ShadowBrush");
	combo_box->addItem(item1);
	
	CEGUI::ListboxTextItem* item2 = new CEGUI::ListboxTextItem("SmashMouth: All Star", 1);
	item2->setSelectionBrushImage("Vanilla-Images/ShadowBrush");
	combo_box->addItem(item2);
	
	CEGUI::ListboxTextItem* item3 = new CEGUI::ListboxTextItem("SmashMouth: Road Man", 1);
	item3->setSelectionBrushImage("Vanilla-Images/ShadowBrush");
	combo_box->addItem(item3);
	
	CEGUI::ListboxTextItem* item4 = new CEGUI::ListboxTextItem("Blink-182: Rock Show", 1);
	item4->setSelectionBrushImage("Vanilla-Images/ShadowBrush");
	combo_box->addItem(item4);
}

void GUIManager::destroyContext()
{
	CEGUI::System& system = CEGUI::System::getSingleton();
	if(gui_context != NULL)
	{
		system.destroyGUIContext(*gui_context);
	}
	
	CEGUI::SchemeManager::getSingleton().destroyAll();
	CEGUI::FontManager::getSingleton().destroyAll();
	
}

CEGUI::OgreRenderer* GUIManager::getRenderer()
{
	static CEGUI::OgreRenderer& renderer = CEGUI::OgreRenderer::bootstrapSystem();
	return &renderer;
}

void GUIManager::keyPressed(std::string game_key)
{
	
}

void GUIManager::mousePressed(uint32 abs_x, uint32 abs_y, std::string button)
{
	CEGUI::MouseButton mouse_enum = CEGUI::NoButton;
	
	if(button == "left")
	{
		mouse_enum = CEGUI::LeftButton;
	}
	else if(button == "right")
	{
		mouse_enum = CEGUI::RightButton;
	}
	else
	{
		mouse_enum = CEGUI::MiddleButton;
	}
	gui_context->injectMouseButtonDown(mouse_enum);
	gui_context->injectMouseButtonUp(mouse_enum);
}

void GUIManager::mouseMoved(uint32 abs_x, uint32 abs_y, int rel_x, int rel_y)
{
	gui_context->injectMouseMove(rel_x, rel_y);
}

void GUIManager::buttonEvent(const CEGUI::EventArgs& e)
{
	CEGUI::Combobox* combo_box = static_cast<CEGUI::Combobox*>(root_window->getChild("FrameWindow/Combobox"));
	CEGUI::String text = combo_box->getText();
	render_manager->guiButtonCallback(text.c_str(), "./assets/scripts/play_audio.lua");

}

void GUIManager::resetEvent(const CEGUI::EventArgs& e)
{
	render_manager->resetMeshes();
}

void GUIManager::loadLevel(std::string level_name)
{
	
	if(gui_context != NULL)
	{
		return;
	}
	
	CEGUI::ImageManager::setImagesetDefaultResourceGroup(level_name);
	CEGUI::Scheme::setDefaultResourceGroup(level_name);
	CEGUI::Font::setDefaultResourceGroup(level_name);
	CEGUI::WidgetLookManager::setDefaultResourceGroup(level_name);
	CEGUI::WindowManager::setDefaultResourceGroup(level_name);
	CEGUI::ScriptModule::setDefaultResourceGroup(level_name);
	
	createContext();
}

void GUIManager::unloadLevel()
{
	if(gui_context != NULL)
	{
		destroyContext();
		gui_context = NULL;
	}
}
















