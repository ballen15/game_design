#include "GameManager.h"
#include "RenderManager.h"
#include "ResourceManager.h"
#include "InputManager.h"
#include "GUIManager.h"
#include "AudioManager.h"
#include "AudioResource.h"
#include "ScriptManager.h"

#include <iostream>

//the problem is that in C++, it is not known when this initialization code will be called
//so we will use a function static variable that is initialized the first time the method is called
//destructor automatically called?
GameManager* GameManager::getGameManager()
{
   static GameManager game_manager;  //initialized when method is called the first time
   return &game_manager;  //won't go out of scope as game_manager is static
}

void GameManager::init()
{
   log_manager = new LogManager("game_log.txt");
   
   resource_manager = new ResourceManager("resources.xml", "scenes.xml", this);
   logComment("Resource Manager Initialized");
   
   render_manager = new RenderManager(this);  //calls render manager's init, sets up the frame listener
   logComment("Render Manager Initialized");
   
   input_manager = new InputManager(this);
   logComment("Input Manager Initialized");
   
   gui_manager = new GUIManager(render_manager);
   logComment("GUI Manager Initialized");
   
   audio_manager = new AudioManager(this);
   logComment("Audio Manager Initialized");
   
   script_manager = new ScriptManager(this);
   logComment("Script Manager Initialized");
   
   
}

GameManager::GameManager()
{
   init();
//   render_manager->parseResources("resources.xml");
//   render_manager->parseScene("scenes.xml");
//   render_manager->buildSimpleScene();
   resource_manager->loadLevel("Sword_Torus");
   render_manager->startRendering();
}

GameManager::~GameManager()
{
	std::cout << "GameManager destructor called" << std::endl;
	delete audio_manager;
	delete input_manager;
	delete render_manager;
	delete resource_manager;
	delete log_manager;	
	resource_manager = NULL;
	render_manager = NULL;
	input_manager = NULL;
	log_manager = NULL;
	audio_manager = NULL;
}

void GameManager::addPathResource(std::string path, std::string level_name)
{
	render_manager->addPathResource(path, level_name);
}

void GameManager::addMeshResource(std::string mesh, std::string level_name)
{
	render_manager->addMeshResource(mesh, level_name);
}

void GameManager::addAudioResource(std::string level_name, std::string file_name, std::string name, int type)
{
	render_manager->addAudioResource(level_name, file_name, name, type);
}

void GameManager::unloadLevel(std::string level_name)
{
	render_manager->unloadLevel(level_name);
}

void GameManager::loadLevel(std::string level_name)
{
	render_manager->loadLevel(level_name);	
}

void GameManager::loadGUI(std::string level_name)
{
	gui_manager->loadLevel(level_name);
}

void GameManager::setupCamera(std::string cam_name, float* cam_position, float* cam_lookat, float* cam_clip)
{
	render_manager->setupCamera(cam_name, cam_position, cam_lookat, cam_clip);
}

void GameManager::setupLight(std::string light_name, std::string type, float* light_color, float* light_direction)
{
	render_manager->setupLight(light_name, type, light_color, light_direction);
}

void GameManager::createEntity(std::string entity_name, std::string node_name, std::string mesh_name, std::string materials_name)
{
	render_manager->createEntity(entity_name, node_name, mesh_name, materials_name);
}

void GameManager::createSceneNode(std::string child_node, std::string parent_node)
{
	render_manager->createSceneNode(child_node, parent_node);
}

void GameManager::processTransformations(int type, float* transform_values, string node_name)
{
	render_manager->processTransformations(type, transform_values, node_name);
}

void GameManager::createAnimation(string animation_name, float animation_time, string animation_mode, string animation_node)
{
	render_manager->createAnimation(animation_name, animation_time, animation_mode, animation_node);
}
void GameManager::createKeyFrame(string animation_name, float keyframe_time, float* key_translate, float* key_rotate, float* key_scale)
{
	render_manager->createKeyFrame(animation_name, keyframe_time, key_translate, key_rotate, key_scale);
}

void GameManager::createAnimationState(std::string animation_name)
{
	render_manager->createAnimationState(animation_name);
}

void GameManager::checkForInput(float time_step)
{
	input_manager->checkForInput(time_step);
}

void GameManager::keyPressed(string keyMap)
{
	render_manager->keyPressed(keyMap);
}

void GameManager::mousePressed(uint32 x_click, uint32 y_click, string mouseMap)
{
	gui_manager->mousePressed(x_click, y_click, mouseMap);
}

void GameManager::mouseMoved(uint32 x_click, uint32 y_click, int x_rel, int y_rel)
{
	gui_manager->mouseMoved(x_click, y_click, x_rel, y_rel);
}

void GameManager::buttonPressed(std::string buttonMap)
{
	render_manager->buttonPressed(buttonMap);
}

void GameManager::rightJoystickMoved(float ns, float ew)
{
	render_manager->rightJoystickMoved(ns, ew);
}

void GameManager::leftJoystickMoved(float ns, float ew)
{
	render_manager->leftJoystickMoved(ns, ew);
}

void GameManager::leftJoystickCallback(float ns, float ew, string event_script)
{
	script_manager->leftJoystickCallback(ns, ew, event_script);
}

AR_Info* GameManager::createAudioInfo()
{
	audio_manager->createAudioInfo();
}

void GameManager::loadAudio(string file_name, AR_Info* arinfo, int type)
{
	audio_manager->loadAudio(file_name, arinfo, type);
}

void GameManager::unloadAudio(AR_Info* arinfo, int type)
{
	audio_manager->unloadAudio(arinfo, type);
}

void GameManager::playAudio(string audio_name)
{
	audio_manager->playAudio(audio_name);
}

void GameManager::updateAudio(float time_step)
{
	audio_manager->updateAudio(time_step);
}

ListArray<AudioResource>* GameManager::getAudioResources()
{
	render_manager->getAudioResources();
}

size_t GameManager::getRenderWindowHandle()
{
   return render_manager->getRenderWindowHandle();
}

void GameManager::logComment(string comment)
{
	log_manager->logComment(comment);
}

void GameManager::logProblem(string message, string file, int line)
{
	log_manager->logProblem(message, file, line);
}

int GameManager::getRenderWindowWidth()
{
   return render_manager->getRenderWindowWidth();
}

int GameManager::getRenderWindowHeight()
{
   return render_manager->getRenderWindowHeight();
}
