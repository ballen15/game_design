#if !defined AUDIO_MANAGER
#define AUDIO_MANAGER

#include "GameHeader.h"
#include "bass.h"
#include <sstream>

class GameManager;
class AudioResource;
struct AR_Info;

class AudioManager
{
	private:
		GameManager* game_manager;
		BASS_DEVICEINFO device_info;
	
	
	public:
		AudioManager(GameManager* gm);
		~AudioManager();
		
		AR_Info* createAudioInfo();
		void init(int Device = 1, DWORD SampleRate = 44100, DWORD flags = 0, HWND win = 0);
		void free();
		void setVolume(float volume){BASS_SetVolume(volume);}
		void pause(){BASS_Pause();}
		void start(){BASS_Start();}
		void updateAudio(float time_step);
		void playAudio(std::string ar);
		void loadAudio(std::string file_name, AR_Info* arinfo, int type);
		void unloadAudio(AR_Info* arinfo, int type);
				

};

#endif