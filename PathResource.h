#if !defined PATH_RESOURCE
#define PATH_RESOURCE

#include "GameResource.h"
class GameManager;
using namespace std;

class PathResource : public GameResource
{
	private:
		GameManager* game_manager;
		
	public:
		PathResource(string level_name, string path, GameResourceType type, GameManager* gm);
		virtual ~PathResource();
		
		virtual void load();
		virtual void unload();

};



#endif