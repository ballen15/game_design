#if !defined INPUT_LISTENER
#define INPUT_LISTENER

#include "RenderListener.h"

class InputRenderListener : public RenderListener
{
	private:
	
	public:
		InputRenderListener(RenderManager* render_manager);
		virtual ~InputRenderListener();
		
		virtual bool frameStarted(const Ogre::FrameEvent& event);	

};

#endif