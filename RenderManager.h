#if !defined RENDER_MANAGER
#define RENDER_MANAGER

#include "Ogre.h"
#include "AnimationRenderListener.h"
#include "InputRenderListener.h"
#include "CSC2110/ListArray.h"

typedef unsigned int uint32;
class GameManager;
class AudioResource;
class ScriptManager;
struct SceneNodeMotion;
struct SceneNodeManual;

class RenderManager
{
   private:
      ListArray<Ogre::AnimationState>* animation_states;	  
	  ListArray<RenderListener>* render_listeners;
	  ListArray<AudioResource>* audio_resources;
	  
      Ogre::Root* root;
      Ogre::RenderWindow* window;
      Ogre::SceneManager* scene_manager;
	  
	  Ogre::Viewport* viewport;
	  Ogre::Camera* cam;

      GameManager* game_manager;
	  ScriptManager* script_manager;
	  AnimationRenderListener* animation_render_listener;
	  InputRenderListener* input_render_listener;

      void init();
      size_t window_handle;
      Ogre::Real time_since_last_frame;

   public:
      RenderManager(GameManager* game_manager);
      virtual ~RenderManager();
	  
	  size_t getRenderWindowHandle();
	  
	  int getRenderWindowWidth();
      int getRenderWindowHeight();
	  
      Ogre::RenderWindow* getRenderWindow();
      Ogre::SceneManager* getSceneManager();
	  
	  void addPathResource(std::string path, std::string level_name);
	  void addMeshResource(std::string mesh, std::string level_name);
	  void addAudioResource(std::string level_name, std::string file_name, std::string name, int type);
	  void unloadLevel(std::string level_name);
	  void loadLevel(std::string level_name);
	  void setupCamera(std::string cam_name, float* cam_position, float* cam_lookat, float* cam_clip);
	  void setupLight(std::string light_name, std::string type, float* light_color, float* light_direction);
	  void createEntity(std::string entity_name, std::string node_name, std::string mesh_name, std::string materials_name);
	  void createSceneNode(std::string child_node, std::string parent_node);
	  void processTransformations(int transform_type, float* transform_values, std::string node_name);
	  void createAnimation(std::string animation_name, float animation_time, std::string animation_mode, std::string animation_node);
	  void createKeyFrame(std::string animation_name, float keyframe_time, float* key_translate, float* key_rotate, float* key_scale);
	  void createAnimationState(std::string animation_name);
	  void processAnimations(float time_step);
	  void checkForInput(float time_step);
	  void keyPressed(std::string keyMap);
	  void mousePressed(uint32 x_click, uint32 y_click, std::string mouseMap);
	  void buttonPressed(std::string buttonMap);
	  void rightJoystickMoved(float ns, float ew);
	  void leftJoystickMoved(float ns, float ew);
	  void updateAudio(float time_step);
	  void playAudio(std::string audio_name);
	  void guiButtonCallback(std::string combo_box_string, std::string script_name);
	  void resetMeshes();
	  ListArray<AudioResource>* getAudioResources();
	  
	  
	  
	  /*
	  void parseResources(const char*);
	  void parseScene(const char*);
	  void processChild(TiXmlElement*, Ogre::SceneNode* parent_node);
	  void processAnimation(TiXmlElement*, Ogre::SceneNode*);	  
	  void processTransformations(int transform_type, std::string& transform_str, Ogre::SceneNode* scene_node);
	  void buildAnimation(Ogre::SceneNode*, Ogre::SceneNode*);
      void buildSimpleScene();
      */

      void startRendering();
      void stopRendering();
};

#endif
