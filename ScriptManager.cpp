#include "ScriptManager.h"
#include "GameManager.h"
#include "GameHeader.h"
#include <iostream>

ScriptManager::ScriptManager(GameManager* gm)
{
	game_manager = gm;

	L = luaL_newstate();
	luaL_openlibs(L);
	
	luabridge::getGlobalNamespace(L)
	.beginClass<GameManager>("GameManager")
		.addFunction("playAudio", &GameManager::playAudio)
		.addFunction("leftJoystickMoved", &GameManager::leftJoystickMoved)
	.endClass();
	
	luabridge::push(L, game_manager);
	lua_setglobal(L, "game_manager");
}

ScriptManager::~ScriptManager()
{
	game_manager = NULL;
}

void ScriptManager::guiButtonCallback(std::string combo_box_text, std::string event_script)
{
	
	luabridge::push(L, combo_box_text.c_str());
	lua_setglobal(L, "name");
	int script = luaL_dofile(L, event_script.c_str());
	
	if(script != 0)
	{
		ASSERT_LOG(script != 0, lua_tostring(L, -1))
		lua_pop(L, 1);
		
	}
}

void ScriptManager::leftJoystickCallback(float ns, float ew, std::string event_script)
{

	luabridge::push(L, ns);
	lua_setglobal(L, "northsouth");
	
	luabridge::push(L, ew);
	lua_setglobal(L, "eastwest");
	
	int script = luaL_dofile(L, event_script.c_str());
	
	if(script != 0)
	{
		ASSERT_LOG(script != 0, lua_tostring(L, -1))
		lua_pop(L, 1);
		
	}
}




















