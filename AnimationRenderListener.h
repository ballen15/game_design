#if !defined ANIMATION_RENDER_LISTENER
#define ANIMATION_RENDER_LISTENER

#include "Ogre.h"
#include "RenderListener.h"
class RenderManager;

class AnimationRenderListener : public RenderListener
{
	private:
	
	public:
		AnimationRenderListener(RenderManager* render_manager);
		virtual ~AnimationRenderListener();		
		bool frameStarted(const Ogre::FrameEvent& event);
	
};

#endif  