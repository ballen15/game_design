#if !defined GAME_RESOURCE
#define GAME_RESOURCE

enum GameResourceType{PATH, MESH, AUDIO};

#include "GameHeader.h"
#include <string>
using namespace std;

class GameResource
{
	protected:
		string resource_level;
		string resource_file;
		GameResourceType resource_type;
		bool is_loaded;
		
	public:
		GameResource(string level_name, string file_name, GameResourceType type);
		virtual ~GameResource();
		
		bool isLoaded();
		
		virtual void load();
		virtual void unload();
		
		string getLevelName();
		string getFileName();
		GameResourceType getResourceType();
		
};

#endif