@echo off
cls

set meshname=%1
set path=c:/Stuff/School/CSC4903/TDM-GCC-64/bin;c:/Stuff/School/CSC4903/ogre_src_v1-8-1/build/bin

OgreXMLConverter %meshname%.mesh.xml
OgreMeshUpgrader %meshname%.mesh
