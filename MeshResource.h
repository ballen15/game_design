#if !defined MESH_RESOURCE
#define MESH_RESOURCE

#include "GameResource.h"
class GameManager;
using namespace std;

class MeshResource : public GameResource
{
	private:
		GameManager* game_manager;
		
	public:
		MeshResource(string level_name, string mesh, GameResourceType type, GameManager* gm);
		virtual ~MeshResource();
		
		virtual void load();
		virtual void unload();
};

#endif